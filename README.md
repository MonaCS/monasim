## Monasim: a Turtlesim-based formation simulator
This package is developed for simulating the formation control algorithms of nonholonomic mobile robots.

### Preparation
#### 1. Setting up the environment
1. Make sure you have a computer with Ubuntu 16.04 (or equivalent) installed.
2. Install **ROS Kinetic Desktop Full** on your computer - follow [this instruction](http://wiki.ros.org/kinetic/Installation/Ubuntu).
3. Create a ROS workspace and build it. In the following instruction, it is assumed that the directory name of the workspace is `catkin_ws`:
    ```
    ~$ cd ~
    ~$ mkdir -p ~/catkin_ws/src
    ~$ cd ~/catkin_ws/
    ~$ catkin_make
    ```
4. Make sure that the source file is updated, use the following command to edit using Terminal:
    ```
    ~$ cd ~
    ~$ sudo nano .bashrc
    ```
    and then write this command at the last line of `.bashrc` file
    ```
    ~$ source ~/catkin_ws/devel/setup.bash
    ```
    Save and close the file.
5. Close all Terminals.

#### 2. Run the simulation
1. Open a Terminal on your computer and go to the source directory of your workspace, and clone this `Monasim` repository, and the build the workspace:
    ```
    ~$ cd ~/catkin_ws/src
    ~$ git clone https://gitlab.com/MonaCS/monasim.git
    ~$ cd ..
    ~$ catkin_make
    ```
2. Run the ROS master:
    ```
    ~$ roscore
    ```
3. Open another Terminal, and launch the *turtlesim* simulator using the provided launch file:
    ```
    ~$ cd ~
    ~$ roslaunch monasim <launch_file>
    ```
    One may choose one of the launch files in `monasim/launch` directory:
    - if only one robot is required, use `<launch_file>=one_turtle.launch`,
    - else if four robots are required, use `<launch_file>=four_turtle.launch`,
    - else if seven robots are required, use `<launch_file>=seven_turtle.launch`.
4. Again, open another Terminal, and execute the ROS node that contains the control algorithm:
    ```
    ~$ cd ~
    ~$ rosrun monasim <control_node>
    ```
    One may choose one of the rosnodes in `monasim/pybasic` or `monasim/pyformation` directory, for example,
    - if executing simple go-to-goal algorithm, use `<control_node>=gotogoal.py`,
    - else if executing consensus algorithm, use `<control_node>=consensus.py`,
    - else if executing affine consensus algorithm, use `<control_node>=affine.py`.

### Maintainer
Hilton Tnunay (htnunay@gmail.com)