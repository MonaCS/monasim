#!/usr/bin/env python
# Description:
# This is the library of the formation control algorithm of multi-agent systems.
# Created by:
# 1. Hilton Tnunay (htnunay@gmail.com)

from __future__ import division
from __future__ import print_function
import math
import numpy as np
import sys

# Update the properties of the network
def updateTopology(N, d):
    global Adjacency
    global Laplacian
    global Stress
    global State
    global Offset
    global Command

    global numberOfAgent
    global numberOfState

    numberOfAgent = N
    numberOfState = d

    Adjacency = np.zeros((numberOfAgent, numberOfAgent))
    Laplacian = np.zeros((numberOfAgent, numberOfAgent))
    Stress = np.zeros((numberOfAgent, numberOfAgent))
    State = np.zeros((numberOfAgent*numberOfState, 1))
    Offset = np.zeros((numberOfAgent*numberOfState, 1))
    Command = np.zeros((numberOfAgent*numberOfState, 1))    

# Set parameters: Adjacency, Laplacian, Stress, states, offset
def setAdjacency(inAdjacency):
    global Adjacency
    Adjacency = inAdjacency

def setLaplacian(inLaplacian):
    global Laplacian
    Laplacian = inLaplacian

def setStress(inStress):
    global Stress
    Stress = inStress

def setState_i(index, inState):
    global State
    State[numberOfState*index : numberOfState*index+(numberOfState)] = inState

def setOffset_i(index, inOffset):
    global Offset
    Offset[numberOfState*index : numberOfState*index+(numberOfState)] = inOffset

def setOffset(inOffset):
    global Offset
    Offset = inOffset

# Get parameters: Adjacency, Laplacian
def getAdjacency():
    return Adjacency

def getLaplacian():
    return Laplacian

def getStress():
    return Stress

# Formation transformation
def doTransformation(index, A, p, b):
    global Offset
    Offset[numberOfState*index : numberOfState*index+(numberOfState)] = np.matmul(A,p) + b

def getRotationMat2D(inTheta):
    theta = math.radians(inTheta)
    return np.array([[math.cos(theta), -math.sin(theta)],[math.sin(theta), math.cos(theta)]])

# Simple proportional controller of agent i
# Input arguments:
# 1. <index> : agent's index
# 2. <kp> : proportional gain
def getProportional(index, kp):
    state = State[numberOfState*index : numberOfState*index+(numberOfState)]
    desPosition = Offset[numberOfState*index : numberOfState*index+(numberOfState)]
    error = state - desPosition
    Command = -np.matmul(kp,error)
    return Command

# Simple consensus controller of agent i
# Input arguments:
# 1. <index> : agent's index
# 2. <kp> : proportional gain
# 3. <velOffset> : velocity offset of agent i
def getConsensus(index, kp, velOffset):
    u = np.zeros([numberOfState,1])
    delta_state = np.zeros([numberOfState,1])
    state_hat_i = np.zeros([numberOfState,1])
    state_hat_j = np.zeros([numberOfState,1])

    state_hat_i = State[numberOfState*index : numberOfState*index+(numberOfState)] - Offset[numberOfState*index : numberOfState*index+(numberOfState)]
    for j in range(numberOfAgent):
        state_hat_j = State[numberOfState*j : numberOfState*j+(numberOfState)] - Offset[numberOfState*j : numberOfState*j+(numberOfState)]
        delta_state = state_hat_i - state_hat_j
        u = u + np.matmul(Adjacency[index, j], delta_state)
    Command = -kp*u	+ velOffset
    # print "k ", Command
    return Command
    
# Calculate consensus controller using matrix
def calculateConsensusController():
    global Command
    state = State[0:numberOfAgent*numberOfState]
    offset = Offset[0:numberOfAgent*numberOfState]
    delta = offset - state

    Command = np.matmul(np.kron(Laplacian, np.eye(numberOfState)), delta)

# Calculate Affine formation controller
# Input arguments:
# 1. <numberOfFollower> : number of agent considered as follower
def calculateAffineController(numberOfFollower):
    global Command
    global Offset

    stress_ff = Stress[0:numberOfFollower, 0:numberOfFollower]
    stress_fl = Stress[0:numberOfFollower, numberOfFollower:numberOfAgent]    

    invStress_ff = np.linalg.inv(stress_ff)
    invStress_ff_fl = np.matmul(invStress_ff, stress_fl)
    aug_stress_ff_fl = np.kron(invStress_ff_fl, np.eye(numberOfState))    

    des_state_l = State[numberOfFollower*numberOfState:numberOfAgent*numberOfState]
    des_state_f = -np.matmul(aug_stress_ff_fl, des_state_l)

    Offset[0 : numberOfFollower*numberOfState] = des_state_f
    Offset[numberOfFollower*numberOfState:numberOfAgent*numberOfState] = State[numberOfFollower*numberOfState:numberOfAgent*numberOfState]

    aug_stress_ff = np.kron(stress_ff, np.eye(numberOfState))
    state_f = State[0:numberOfFollower*numberOfState]
    delta_state_f = state_f - des_state_f
    Command = np.matmul(aug_stress_ff, delta_state_f)

# Get formation controller of agent i
# Input arguments:
# 1. <index> : agent's index
# 2. <kp> : proportional gain
# 3. <velOffset> : velocity offset of agent i
def getFormationControlCommand(index, kp, velOffset):
    command = kp*Command[numberOfState*index : numberOfState*index+numberOfState] + velOffset
    return command

# Calculate state error
def calculateStateError():
    position_e = State - Offset
    position_e_norm = np.linalg.norm(position_e, 2)
    return position_e_norm

# Get formation controller with feedback linearisation
# Input arguments:
# 1. <alpha> : linear velocity control gain
# 2. <beta> : angular velocity control gain
# 3. <u> : raw control input (control input that has not been fed to a robot dynamic/kinematic)
# 4. <theta> : orientation of the mobile robot
# 5. <RRobot> : center to head radius of the robot
# 6. <limitVel> : linear velocity limit
# 7. <limitOmega> : angular velocity limit
def getControllerKinematics(alpha, beta, u, theta, RRobot, limitVel, limitOmega):    
    v = alpha*u[0]*math.cos(theta) + alpha*u[1]*math.sin(theta)
    w = beta*(-(u[0]/RRobot)*math.sin(theta) + (u[1]/RRobot)*math.cos(theta))
    
    if v > limitVel:
        v = limitVel
    elif v < -limitVel:
        v = -limitVel
    if w > limitOmega:
        w = limitOmega
    elif w < -limitOmega:
        w = -limitOmega

    return np.array([[v], [w]])

